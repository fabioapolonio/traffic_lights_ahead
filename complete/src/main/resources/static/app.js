var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
}

function connect() {
    var socket = new SockJS('/traffic-lights-ahead');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/status', function (status) {
            showStatus(JSON.parse(status.body).status);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function showStatus(message) {

    if (message == 'GREEN'){
      $("#divg").removeClass("typeGreen");
      $("#divg").addClass("typeActiveGreen");

      $("#divr").removeClass("typeActiveRed");
      $("#divr").addClass("typeRed");

    } else if (message == 'AMBER'){
      $("#diva").removeClass("typeAmber");
      $("#diva").addClass("typeActiveAmber");

      $("#divg").removeClass("typeActiveGreen");
      $("#divg").addClass("typeGreen");

    } else {
      $("#divr").removeClass("typeRed");
      $("#divr").addClass("typeActiveRed");

      $("#diva").removeClass("typeActiveAmber");
      $("#diva").addClass("typeAmber");
    }

}
$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
});

