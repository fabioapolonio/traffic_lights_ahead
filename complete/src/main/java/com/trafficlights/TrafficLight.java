package com.trafficlights;

public class TrafficLight {
    private TrafficLightColor status = TrafficLightColor.RED;

    public void changeStatus(){

        if (status == TrafficLightColor.GREEN)
            status = TrafficLightColor.AMBER;
        else if (status == TrafficLightColor.AMBER)
            status = TrafficLightColor.RED;
        else
            status = TrafficLightColor.GREEN;

    }
    public TrafficLightColor getStatus() {
        return status;
    }
}
