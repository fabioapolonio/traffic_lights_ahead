package com.trafficlights;

public enum TrafficLightColor {
    GREEN(0),
    AMBER(1),
    RED(2);

    private int color;

    TrafficLightColor(int color) {
        this.color = color;
    }

    public int getColor(){
        return this.color;
    }
}
