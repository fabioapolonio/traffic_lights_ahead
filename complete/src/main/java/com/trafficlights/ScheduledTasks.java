package com.trafficlights;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;

@Component
public class ScheduledTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private SimpMessagingTemplate template;

    @Resource
    private TrafficLightService trafficLightService;


    @Scheduled(fixedRate = 2000)
    public void changeLight() throws Exception {
        template.convertAndSend("/topic/status",trafficLightService.sendTrafficListStatus());

    }
}