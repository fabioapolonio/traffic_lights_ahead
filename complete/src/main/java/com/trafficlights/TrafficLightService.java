package com.trafficlights;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;

@Service
public class TrafficLightService {
    private static final Logger log = LoggerFactory.getLogger(TrafficLightService.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Resource(name = "tl1")
    private TrafficLight trafficLight;

    public TrafficLight sendTrafficListStatus(){
        trafficLight.changeStatus();
        log.info("Traffic light state: {}",trafficLight.getStatus());
        return trafficLight;
    }
}
